import React from 'react';

const Listing = ({items = []}) => {

    const formatPrice = (price, currency) => (
        currency === 'USD' ? `$${price}` : (currency === 'EUR' ? `€${price}` : `${price} GPB`)
    );

    const getLevelClass = quantity => (
        quantity < 10 ? 'level-low' : quantity < 20 ? 'level-medium' : 'level-high'
    );

    const formatTitle = title => title.length > 50 ? `${title.slice(0, 50)}...` : title;

    return (
        <div className="item-list">
            {items.map(({state, listing_id, url, title, price, quantity, currency_code, MainImage}) => (
                state === 'removed'
                    ? null
                    : <div className="item" key={listing_id}>
                        <div className="item-image">
                            <a href={url}>
                                <img src={MainImage.url_570xN} alt=""/>
                            </a>
                        </div>
                        <div className="item-details">
                            <p className="item-title">{formatTitle(title)}</p>
                            <p className="item-price">{formatPrice(price, currency_code)}</p>
                            <p className={"item-quantity " + getLevelClass(quantity)}>{quantity} left</p>
                        </div>
                    </div>
            ))}
        </div>
    )
};

export default Listing;