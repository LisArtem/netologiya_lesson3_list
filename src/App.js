import React from 'react';
import Listing from "./components/Listing";
import data from './data/etsy.json';
import './style.css';

const App = () => (
    <div className="App">
        <Listing items={data}/>
    </div>
);

export default App;